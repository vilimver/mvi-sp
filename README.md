# Semestral work NI-MVI
##### Veronika Vilímovská


### Assignment
Develop a multiclass regression model capable of learning and predicting computational texture image statistics. 

These statistics should range from basic attributes like image brightness, shininess, or contrast to more complex ones such as the number of main colors, pattern directionality, or regularity. 

The source data for this model will consist of a dataset comprising texture images alongside their known statistical values. 

The model's objective is to predict these values accurately.
Your task involves designing, creating, and rigorously testing this multiclass regression model. Following the model's creation, evaluate the quality of predictions for each individual statistic tested. Engage in a thorough discussion to explore potential reasons for the model's performance and suggest viable methods to enhance the accuracy of its predictions.

### Directions for data acquisition
Train and test data comprise of image data from the database of UTIA AV and statistics manually computed on these images. The data can be accessed upon request. Data used in a notebook of examples are attached in a directory __examples_data__.

### Directions for launch
Examples of how to launch the models are attached in the notebook __example_predictions.ipynb__ (or can be viewed in the HTML version of the notebook).

The work is using pretrained models of AlexNet and GoogleNet from the PyTorch hub. These are downloaded and then the last part replaced (classifier for AlexNet, fc for GoogleNet) with Linear layers with the usage of ReLU function as you can see in the notebook or the report.pdf. Pretrained parameters for these adjusted models are attached in the __models_parameters__ directory and need to replace those in the pretrained models (with the *model.load_state_dict* function). 

Main thing, that needs to be adjusted to launch models and other notebooks, are paths to files.

Input into these models is an image, that neeeds to be preprocessed (the process is showed and compiled into a function in the notebook), output is a tensor with 4 numbers.
